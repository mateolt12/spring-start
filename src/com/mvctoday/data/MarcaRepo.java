package com.mvctoday.data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.mvctoday.model.Marca;

@Repository
public class MarcaRepo {

	private Map<Integer, Marca> marcas;
	
	public MarcaRepo() {
		this.marcas = new HashMap<Integer, Marca>();
		
		this.marcas.put(1, new Marca(1, "Hoyt Archery", "https://hoyt.com", "EEUU"));
		this.marcas.put(2, new Marca(2, "Easton Archery", "https://eastonarchery.com", "EEUU"));
		this.marcas.put(3, new Marca(3, "Cartel Doosung", "http://www.doosungarchery.co.kr/eng/", "Corea del sur"));
		this.marcas.put(4, new Marca(4, "Bear Archery", "https://beararchery.com", "EEUU"));
	}
	
	public List<Marca> getAllMarcas(){
		return new LinkedList<Marca>(marcas.values());
	}
}
