package com.mvctoday.helloworld;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

	@RequestMapping("/hello")
	public String sayHello(Model model) {
		model.addAttribute("saludo", "Hola Mundo!");
		model.addAttribute("mensaje", "Me llena de orgullo y satisfacción que me comais los cojones.");
		model.addAttribute("url", "http://youtube.com");
		return "hello";

	}

}
