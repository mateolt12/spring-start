package com.mvctoday.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mvctoday.data.MarcaRepo;
import com.mvctoday.model.Marca;

@Service
public class SimpleMarcaService implements MarcaService{
	
	@Autowired
	private MarcaRepo marcaRepository;

	@Override
	public List<Marca> getMarcas() {
		// TODO Auto-generated method stub
		return marcaRepository.getAllMarcas();
	}

}
