package com.mvctoday.services;

import java.util.List;

import com.mvctoday.model.Marca;

public interface MarcaService {

	public List<Marca> getMarcas();
	
}
