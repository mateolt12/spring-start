package com.mvctoday.model;

public class Marca {
	
	private int id;
	private String name;
	private String web;
	private String location;
	
	public Marca() {}
	
	public Marca(int id, String name, String web, String location) {
		this.id = id;
		this.name = name;
		this.web = web;
		this.location = location;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
}
